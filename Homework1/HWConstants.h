//
//  HWConstants.h
//  Homework1
//
//  Created by Vladislav Grigoriev on 27/09/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#ifndef HWConstants_h
#define HWConstants_h

static CGFloat const HWDefaultMargin = 16.0f;
static CGFloat const HWDefaultInputHeight = 50.0f;
static CGFloat const HWDefaultButtonWidht = 100.0f;
static CGFloat const HWDefaultFontSize = 12.0f;

#endif /* HWConstants_h */
